import '../App';
import React from 'react';
const initialArray = Array(9).fill('');

function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
}

function TicTacToe() {
  const [first, setFirst] = React.useState(initialArray);
  const [isplay, updatePlay] = React.useState(false);
  const [isWinner, updateWinner] = React.useState('');
  const [Winner, ActualWinner] = React.useState('Game on progress...');

  function changeBox(index) {
    if (initialArray[index] === '' && !isWinner) {
      initialArray[index] = isplay ? 'X' : 'O';
      updatePlay(!isplay);
      setFirst(initialArray);
      let winner = calculateWinner(first);
      if (winner) {
        updateWinner(winner);
        ActualWinner(`${winner} win the Game`);
      }
    }
  }
  function restart() {
    initialArray.fill('');
    ActualWinner('Game on progress');
    setFirst(initialArray);
    updatePlay(!isplay);
    updateWinner('');
  }
  return (
    <div className="App">
      <div className="Main">
        <div className="FirstRow">
          <div className="First Block" onClick={() => changeBox(0)}>
            {first[0]}
          </div>
          <div className="Second Block" onClick={() => changeBox(1)}>
            {first[1]}
          </div>
          <div className="Third Block" onClick={() => changeBox(2)}>
            {first[2]}
          </div>
        </div>
        <div className="SecondRow">
          <div className="First Block" onClick={() => changeBox(3)}>
            {first[3]}
          </div>
          <div className="Second Block" onClick={() => changeBox(4)}>
            {first[4]}
          </div>
          <div className="Third Block" onClick={() => changeBox(5)}>
            {first[5]}
          </div>
        </div>
        <div className="ThirdRow">
          <div className="First Block" onClick={() => changeBox(6)}>
            {first[6]}
          </div>
          <div className="Second Block" onClick={() => changeBox(7)}>
            {first[7]}
          </div>
          <div className="Third Block" onClick={() => changeBox(8)}>
            {first[8]}
          </div>
        </div>
        <div className="Winner">{Winner}</div>
        <button className="Button" onClick={restart}>
          Restart Game
        </button>
      </div>
    </div>
  );
}

export default TicTacToe;
